# frozen_string_literal: true

module CloudParty
  module Responses
    autoload :Memberships, 'cloud_party/responses/memberships'
    autoload :IPs, 'cloud_party/responses/ips'
  end
end
