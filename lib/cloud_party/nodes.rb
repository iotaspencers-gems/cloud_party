# frozen_string_literal: true

module CloudParty
  module Nodes
    autoload :Memberships, 'cloud_party/nodes/memberships'
    autoload :IPs, 'cloud_party/nodes/ips'
  end
end
