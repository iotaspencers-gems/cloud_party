# frozen_string_literal: true

require 'cloud_party/exception'
module CloudParty
  autoload :BadRequestError, 'cloud_party/exceptions/bad_request_400'
end
