# frozen_string_literal: true

module CloudParty
  module Responses
    module Node
      autoload :Account, 'cloud_party/responses/nodes/account'
      autoload :Permissions, 'cloud_party/responses/nodes/permissions'
    end
  end
end
